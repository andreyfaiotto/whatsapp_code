# MANUAL DE IMPLEMENTAÇÃO DO SCRIPT DE WHATSAPP EM QUALQUER SITE

Este documento tem como finalidade instalar tag de whatsapp com snippet de conversão em qualquer site, via código ou via GTM. Siga a ordem de implementação dos trechos de código.

## CONFIGURAÇÕES DISPONÍVEIS

Dentro da variável ``` configuration ``` você vai encontrar alguns valores pré definidos, altere para o valor que precisar, seguindo esta explicação.

**PHONE_NUMBER**

Representado por ```configuration.phone_number```

Altere para o número desejado, sempre mantendo o código do pais e o prefixo da cidade



**WHATSAPP_TEXT**

Representado por ```configuration.whatsapp_text```

Modifique o texto do whatsapp para qual desejar, sempre mantendo as aspas entre ele.



**URL_OBRIGADO**

Representado por ```configuration.url_obrigado```

Caso queria redirecionar o cliente para uma outra pagina, adicione o link neste espaço, sempre dentro de aspas, como o texto.


---

## PASSO A PASSO

1. Adicione os scripts de Analytics à sua aplicação. Não vou adicionar eles aqui por questão de segurança.

2. Adicione o script personalizado dentro da tag ```<head></head>``` do seu site, abaixo dos scripts do Google Analytics e troque o numero de telefone para as informações necessárias.


``` 
<script>
    document.addEventListener("DOMContentLoaded",function(){
        const configuration = {
            phone_number: 551199999999,
            whatsapp_text: "Clique no botão ao lado para falar com a Vida Sonora",
            url_obrigado: undefined
        }
        document.querySelector('body').insertAdjacentHTML('beforeend', '<a href="https://api.whatsapp.com/send?phone='+ configuration.phone_number +'&text='+configuration.whatsapp_text+'" class="sp-floating-btn" target="_blank"></a>');
        document.querySelector('.sp-floating-btn').addEventListener('click', function(){
            gtag_report_conversion(configuration.url_obrigado);
        });
    })
</script>
```


3. Adicione este estilo entre a tag ```<head></head>``` do seu site.


```
<style>
    a.sp-floating-btn {
        position: fixed;
        z-index: 99999 !important;
        bottom: 10px !important;
        right: 10px !important;
        background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pjxzdmcgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNjQgNjQ7IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA2NCA2NCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PHN0eWxlIHR5cGU9InRleHQvY3NzIj4KCS5zdDB7ZmlsbDojNDI2N0IyO30KCS5zdDF7ZmlsbDp1cmwoI1NWR0lEXzFfKTt9Cgkuc3Qye2ZpbGw6I0ZGRkZGRjt9Cgkuc3Qze2ZpbGw6I0MyMTkxRTt9Cgkuc3Q0e2ZpbGw6IzFEQTFGMzt9Cgkuc3Q1e2ZpbGw6I0ZFRkUwMDt9Cgkuc3Q2e2ZpbGw6IzI1RDM2NjtzdHJva2U6I0ZGRkZGRjtzdHJva2Utd2lkdGg6NTtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cgkuc3Q3e2ZpbGw6I0NCMjAyNzt9Cgkuc3Q4e2ZpbGw6IzAwNzdCNTt9Cgkuc3Q5e2ZpbGw6dXJsKCNTVkdJRF8yXyk7fQoJLnN0MTB7ZmlsbDp1cmwoI1NWR0lEXzNfKTt9Cgkuc3QxMXtmaWxsOiNGRjAwNEY7fQoJLnN0MTJ7ZmlsbDojMDBGN0VGO30KCS5zdDEze2ZpbGw6IzUxODFCODt9Cgkuc3QxNHtmaWxsOiMzOTU5NzY7fQoJLnN0MTV7ZmlsbDojRjU4MjIwO30KCS5zdDE2e2ZpbGw6I0U2MTYyRDt9Cgkuc3QxN3tmaWxsOiNGRjk5MzM7fQo8L3N0eWxlPjxnIGlkPSJndWlkbGluZXMiLz48ZyBpZD0iRkIiLz48ZyBpZD0iaWciLz48ZyBpZD0ieXQiLz48ZyBpZD0idHdpdHRlciIvPjxnIGlkPSJzbmFwY2hhdCIvPjxnIGlkPSJXQSI+PGc+PHBhdGggY2xhc3M9InN0NiIgZD0iTTUsNTlsMTItMy4zYzQuMywyLjcsOS41LDQuMywxNSw0LjNjMTUuNSwwLDI4LTEyLjUsMjgtMjhTNDcuNSw0LDMyLDRTNCwxNi41LDQsMzJjMCw1LjUsMS42LDEwLjcsNC4zLDE1ICAgIEw1LDU5eiIvPjxwYXRoIGNsYXNzPSJzdDIiIGQ9Ik00NS45LDM5LjZjLTEuOSw0LTUuNCw0LjUtNS40LDQuNWMtMywwLjYtNi44LTAuNy05LjgtMi4xYy00LjMtMi04LTUuMi0xMC41LTkuM2MtMS4xLTEuOS0yLjEtNC4xLTIuMi02LjIgICAgYzAsMC0wLjQtMy41LDMtNi4zYzAuMy0wLjIsMC42LTAuMywxLTAuM2wxLjUsMGMwLjYsMCwxLjIsMC40LDEuNCwxbDIuMyw1LjZjMC4yLDAuNiwwLjEsMS4yLTAuMywxLjdsLTEuNSwxLjYgICAgYy0wLjUsMC41LTAuNSwxLjItMC4yLDEuOGMwLjEsMC4yLDAuMywwLjUsMC42LDAuOGMxLjgsMi40LDQuMiw0LjIsNi45LDUuNGMwLjQsMC4yLDAuNywwLjMsMSwwLjRjMC43LDAuMiwxLjMtMC4xLDEuNy0wLjYgICAgbDEuMi0xLjhjMC4zLTAuNSwwLjktMC44LDEuNS0wLjdsNiwwLjljMC42LDAuMSwxLjEsMC42LDEuMywxLjJsMC40LDEuNUM0NiwzOC45LDQ2LDM5LjMsNDUuOSwzOS42eiIvPjwvZz48L2c+PGcgaWQ9IlBpbnRlcnJlc3QiLz48ZyBpZD0iTGF5ZXJfOSIvPjxnIGlkPSJMYXllcl8xMCIvPjxnIGlkPSJMYXllcl8xMSIvPjxnIGlkPSJMYXllcl8xMiIvPjxnIGlkPSJMYXllcl8xMyIvPjxnIGlkPSJMYXllcl8xNCIvPjxnIGlkPSJMYXllcl8xNSIvPjxnIGlkPSJMYXllcl8xNiIvPjxnIGlkPSJMYXllcl8xNyIvPjwvc3ZnPg==');
        display: block;
        width: 50px;
        height: 50px;
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 50%;
    }
</style>
```


---

Siga os passos e cole exatamente como esta nesta pasta.

Caso precise, [clique aqui e veja nosso exemplo de implementação](https://bitbucket.org/andreyfaiotto/whatsapp_code/src/master/codes-base64.html)


## IMPLEMENTAÇÃO COM O SVG


Caso queria que o elemento do botão whatsapp seja um SVG, [clique aqui e veja o código de impleentação usando o base64](https://bitbucket.org/andreyfaiotto/whatsapp_code/src/master/codes.html)

valeu!